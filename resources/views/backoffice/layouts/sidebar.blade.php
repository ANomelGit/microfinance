<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
      <img  style="width:260px;" alt="Yes Logo" src="{{ url('main/assets/images/Sanstitre-121.png') }}">
      <h2 style="font-size:15px;font-weight:900; color:darkgreen; text-align:center;">IVOIRE BANQUE ADMIN</h2>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item {{ $title == 'Tableau de bord' ? 'active' : '' }}">
          <a class="nav-link" href="{{ url('/dashboard') }}">
            <i class="material-icons">dashboard</i>
            <p>Tableau de bord</p>
          </a>
        </li>
        <li class="nav-item {{ $title == 'Compte client' ? 'active' : '' }}">
            <a class="nav-link" data-toggle="collapse" href="#formsExamples" aria-expanded="false">
              <i class="material-icons">person</i>
            <p>Compte client <b class="caret"></b></p>
            </a>
            <div class="collapse {{ $title == 'Compte client' ? 'show' : '' }}" id="formsExamples">
              <ul class="nav">
                <li class="nav-item {{ $sub_title == 'Ajouter client' ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('ajouterClient') }}">
                    <span class="sidebar-mini"><i class="material-icons">add</i></span>
                    <span class="sidebar-normal"> Ajouter </span>
                  </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="regular.html">
                      <span class="sidebar-mini"><i class="material-icons">view_headline</i></span>
                      <span class="sidebar-normal"> Liste des clients </span>
                    </a>
                  </li>
              </ul>
            </div>
          </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ url('/typeoperation') }}">
            <i class="material-icons">content_paste</i>
            <p>Operation</p>
          </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="{{ url('/ListBanque') }}">
            <i class="material-icons">library_books</i>
            <p>Agence</p>
          </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="{{ url('/ListBanque') }}">
            <i class="material-icons">bubble_chart</i>
            <p>Ma Banque</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
