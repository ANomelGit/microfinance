<!--Navbar -->
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top" style="background-color:orange; position:fixed;margin-left:260px; width:auto;">
  <div class="container-fluid">
    <div class="navbar-wrapper" style="margin-left:10px;">
      <i class="material-icons">dashboard</i>
      <a class="navbar-brand" style="font-size:30px;" href="javascript:;">{{ $title }}</a>
    </div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-55">
      <ul class="navbar-nav ml-auto nav-flex-icons">
        <li class="nav-item avatar dropdown">
          <a class="nav-link dropdown-toggle"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="{{ url('main/assets/images/Sanstitre-121.png') }}" style="width:50px; height:50px; border:1px solid black; border-radius:50%; class="avatar z-depth-0" alt="avatar image">
          </a>
          <div class="dropdown-menu dropdown-menu-lg-right dropdown-success">
            <a class="dropdown-item"data-toggle="modal" data-target="#modalLoginForm">Parametres</a>
            <a class="dropdown-item" href="">Deconnexion</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!--modal show for user parametre-->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="" method="post" enctype="multipart/form-data">
        <div class="modal-header infouser text-center">
          <div class="profil">
            <img src="{{ url('main/assets/images/Sanstitre-121.png') }}" style="width:60px; height:60px;border:1px solid black; border-radius:50%;" alt="image">
          </div>
          <div >
            <h4 class="modal-title w-100 font-weight-bold">Paramètres</h4><hr/>
            <div class="file-field medium">
              <div class="file-field">
                <div class="btn btn-outline-success btn-rounded waves-effect btn-sm float-left">
                  <span><input class="file-path validate fil-path" type="text" placeholder="Changez votre photo de profil"></span>
                  <input type="file" name="photoprofil" value="2 * 1024 * 1024">
                </div>
              </div>
            </div>
          </div>
          <div class="closed">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        <div class="modal-body mx-3">
          <div class="md-form">
            <label data-error="wrong" data-success="right">Nom</label>
            <input type="text" id="FirstNameAdmin" name="FirstNameAdmin" class="form-control validate"  placeholder="Non de famille" >
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right" >Prenoms</label>
            <input type="text" id="OtherNameAmin" name="OtherNameAdmin" class="form-control validate" placeholder="Prenoms">
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right">Mot de passe</label>
            <input type="password" id="Password" name="Password" class="form-control validate" placeholder="Entrer votre nouveau Password">
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right">Confirmer votre nouveau Password</label>
            <input type="password" id="confPassword" name="confPassword" class="form-control validate" placeholder="Confirmer votre nouveau Password">
          </div>
          <center><input type="submit" name="changeParamUser" value ="Sauvegarder ses changements" class="btn btn-success buttonAdd"></center>
        </div>
      </form>
    </div>
  </div>
</div>