@extends('backoffice/layouts/master', [
'title' => 'Different type operation',
'sub_title' => '',
])
@section('content')


<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-4">
            <div class="card text-center ">
               <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon" style="background:darkorange;">
                     <i class="fa fa-arrow-circle-down fa-3x"></i>
                  </div>
               </div>
               <div class="card-body">
                  <h2><a href="{{ url('/opdepot') }}"> DEPÔT</a></h2>
               </div>
               <div class="card-footer "style="background:darkorange;"></div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="card text-center ">
               <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon" style="background:darkorange;">
                     <i class="fa fa-arrow-circle-down fa-3x"></i>
                  </div>
               </div>
               <div class="card-body">
                  <h2> <a href="{{ url('/opretrait') }}"> RETRAIT</a></h2>
               </div>
               <div class="card-footer" style="background:darkorange;"></div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="card text-center ">
               <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon" style="background:darkorange;">
                     <i class="fa fa-arrow-circle-down fa-3x"></i>
                  </div>
               </div>
               <div class="card-body">
               <h2><a href="{{ url('/optransfert') }}"> TRANSFERT</a></h2>
               </div>
               <div class="card-footer" style="background:darkorange;"></div>
            </div>
         </div>
      </div>
   </div>
</div>


@endsection
