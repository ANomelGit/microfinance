@extends('backoffice/layouts/master', [
'title' => 'Transfert',
'sub_title' => '',
])
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                        <div class="card">
                                <div class="card-header">TRANSFERT</div>
                                        <div class="card-body">
                                                <form>
                                                        <div class="row">
                                                                <div class="col-md-4">
                                                                        <div class="form-group">
                                                                        <label class="bmd-label-floating">Numéro de compte client</label>
                                                                                <input type="text" class="form-control">
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                        <div class="form-group">
                                                                        <label class="bmd-label-floating">Nom</label>
                                                                                <input type="text" class="form-control">
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                        <div class="form-group">
                                                                        <label class="bmd-label-floating">Prénom</label>
                                                                                <input type="text" class="form-control">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="row">
                                                                <div class="col-md-3">
                                                                        <div class="form-group">
                                                                        <label class="bmd-label-floating">Montant à deposer</label>
                                                                                <input type="text" class="form-control">
                                                                        </div>
                                                                </div>
                                                                
                                                                <div class="col-md-3">
                                                                        <div class="form-group">
                                                                                <select class="form-control" id="sel1">
                                                                                        <option>Virement</option>
                                                                                </select>
                                                                        </div>
                                                                </div>
                                                        </div>
                      
                     
                                                        <div class="col-md-4">
                                                                <div class="card card-profile">
                                                                        <div class="card-body">
                                                                                <div class="file-field medium">
                                                                                        <div class="file-field">
                                                                                                <div class="btn btn-outline-success btn-rounded waves-effect btn-sm float-left">
                                                                                                        <label class="bmd-label-floating">Signature</label><br/>
                                                                                                        <input type="file" name="photoprofil" value="2 * 1024 * 1024">
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                      
                    
                                                                
                                                
                                                       
                                                        <button type="submit" class="col-md-2 btn btn-success pull-right"> Valider </button>
                                                        <a href="{{ url('/typeoperation') }}" class="col-md-2 btn btn-danger pull-right"> Annuler </a>


                                                </form>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
@endsection
