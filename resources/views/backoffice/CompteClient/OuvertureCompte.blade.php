@extends('backoffice/layouts/master', [
'title' => 'Ouverture de Compte',
'sub_title' => '',
])
@section('content')



<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <form>
                  <div class="row">
                    <div class="col-md-8">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating">Nom</label>
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating">Prenom</label>
                            <input type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="bmd-label-floating">Adresse</label>
                            <input type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating">Date de naissance</label>
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <select class="form-control" id="sel1">
                              <option>sexe</option>
                              <option>M</option>
                              <option>F</option>
                              <option>autre</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating">Telephone</label>
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating">Email</label>
                            <input type="email" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Observation</label>
                            <div class="form-group">
                              <label class="bmd-label-floating"> Veillez saisir le texte ici</label>
                              <textarea class="form-control" rows="5"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card card-profile">
                            <div class="card-body">
                              <p class="card-description">
                                <div class="file-field medium">
                                  <div class="file-field">
                                    <div class="btn btn-outline-success btn-rounded waves-effect btn-sm float-left">
                                      <label class="bmd-label-floating">Photo de profile</label><br/>
                                      <input type="file" name="photoprofil" value="2 * 1024 * 1024">
                                    </div>
                                  </div>
                                </div>
                              </p>  
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card card-profile">
                            <div class="card-body">
                              <div class="file-field medium">
                                <div class="file-field">
                                  <div class="btn btn-outline-success btn-rounded waves-effect btn-sm float-left">
                                  <label class="bmd-label-floating">Signature</label><br/>
                                    <input type="file" name="photoprofil" value="2 * 1024 * 1024">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a href="#"  style="color:white;"><button type="submit" class="btn btn-success pull-right">Valider </button></a>
                  <a href="{{ url('/dashboard') }}"  style="color:white;" class="btn btn-danger pull-right"> Retour</a>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
