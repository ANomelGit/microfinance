@extends('backoffice/layouts/master', [
'title' => 'Compte client',
'sub_title' => 'Ajouter client',
])

@section('content')
<div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-rose card-header-text">
                    <div class="card-text">
                      <h4 class="card-title">Informations personnelles</h4>
                    </div>
                  </div>
                  <div class="card-body ">
                    <form method="post" enctype="multipart/form-data" action="{{ route('client.infosPersonnelles') }}" class="form-horizontal">
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Nom</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="text" value="{{ old('nom', Session::get('nom')) }}" name="nom" required class="form-control">
                            <span class="text-danger bmd-help">
                                @error('nom')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Prénom(s)</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="text" name="pnom" value="{{ old('pnom', Session::get('pnom')) }}" required class="form-control">
                            <span class="text-danger bmd-help">
                                @error('pnom')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Date de naissance</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="date" name="datenaiss" value="{{ old('datenaiss', Session::get('datenaiss')) }}" required max="2003-01-01" class="form-control" placeholder="Date de naissance">
                            <span class="text-danger bmd-help">
                                @error('datenaiss')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Sexe</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <select name="sexe" required class="form-control">
                                <option selected disabled>Veuillez choisir le sexe</option>
                                <option value="H" @if (old('sexe', Session::get('sexe')) == 'H') selected="selected" @endif>Homme</option>
                                <option value="F" @if (old('sexe', Session::get('sexe')) == 'F') selected="selected" @endif>Femme</option>
                            </select>
                            <span class="text-danger bmd-help">
                                @error('sexe')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="email" value="{{ old('email', Session::get('email')) }}" required name="email" class="form-control">
                            <span class="text-danger bmd-help">
                                @error('email')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Téléphone</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="tel" value="{{ old('tel', Session::get('tel')) }}" name="tel" required min="10" max="10" class="form-control">
                            <span class="text-danger bmd-help">
                                @error('tel')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Carte Nationale d'identité (CNI)</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="text" name="cni" value="{{ old('cni', Session::get('cni')) }}" required class="form-control">
                            <span class="text-danger bmd-help">
                                @error('cni')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Profession</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="text" value="{{ old('profession', Session::get('profession')) }}" name="profession" required class="form-control">
                            <span class="text-danger bmd-help">
                                @error('profession')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Photo</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                              <div class="fileinput-new thumbnail">
                                <img src="https://demos.creative-tim.com/material-dashboard-pro/assets/img/image_placeholder.jpg" alt="...">
                              </div>
                              <div class="fileinput-preview fileinput-exists thumbnail"></div>
                              <div>
                                <span class="btn btn-rose btn-round btn-file">
                                  <span class="fileinput-new">Selectionner image</span>
                                  <span class="fileinput-exists">Changer</span>
                                  <input type="file" required name="photo" accept="image/*"/>
                                  <span class="text-danger bmd-help">
                                    @error('photo')
                                        {{ $message }}
                                    @enderror
                                </span>
                                </span>
                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Signature</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                              <div class="fileinput-new thumbnail">
                                <img src="https://demos.creative-tim.com/material-dashboard-pro/assets/img/image_placeholder.jpg" alt="...">
                              </div>
                              <div class="fileinput-preview fileinput-exists thumbnail"></div>
                              <div>
                                <span class="btn btn-rose btn-round btn-file">
                                  <span class="fileinput-new">Selectionner image</span>
                                  <span class="fileinput-exists">Changer</span>
                                  <input type="file" required name="signature" accept="image/*"/>
                                  <span class="text-danger bmd-help">
                                    @error('signature')
                                        {{ $message }}
                                    @enderror
                                </span>
                                </span>
                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                        <label class="col-sm-2 col-form-label">Observation</label>
                        <div class="col-sm-10">
                          <div class="form-group">
                            <textarea name="obs" value="{{ old('obs', Session::get('obs')) }}" cols="30" rows="10" class="form-control"></textarea>
                            <span class="text-danger bmd-help">
                                @error('obs')
                                    {{ $message }}
                                @enderror
                            </span>
                          </div>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary pull-right">Suivant</button>
                      <div class="clearfix"></div>
                    </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
</div>
@endsection
