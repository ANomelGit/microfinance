@extends('backoffice/layouts/master', [
'title' => 'Compte Client',
'sub_title' => '',
])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <form class="navbar-form">
          <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
              <i class="material-icons">search</i>
              <div class="ripple-container"></div>
            </button>
          </div>
        </form>
      </div>
      <div class="col-md-4">
        <a href="{{ url('/openAccount') }}">
          <button data-mdb-ripple-color="dark" type="submit" class="btn btn-success pull-right">Céer compte</button>
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header" style="background:darkorange; color:white;">
            <h4 class="card-title ">Liste des comptes</h4>
          </div>-
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>Photo</th>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Date naiss</th>
                  <th>Telephone</th>
                  <th>Email</th>
                  <th>Adresse</th>
                  <th>Profession</th>
                  <th>Signature</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="card-avatar">
                        <a href="javascript:;"><img class="img" src="{{ url('main/assets/images/Sanstitre-121.png') }}" style="width:60px; height:60px;border:1px solid black; border-radius:50%;" alt="image"/></a>
                      </div>
                    </td>
                    <td>Dakota</td>
                    <td>mamba</td>
                    <td>20/05/2000</td>
                    <td>58915660</td>
                    <td>$mamba@gmail.com</td>
                    <td>treichville</td>
                    <td>etudiante</td>
                    <td class="text-primary">$36,738</td>
                    <td class="text-primary">
                    <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                      <i class="material-icons dropdown-item"data-toggle="modal" data-target="#modalmodifForm">edit</i>
                    </button>
                    <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                      <i class="material-icons dropdown-item"data-toggle="modal" data-target="#modalsuppForm">close</i>
                    </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!--modal show for user parametre-->
<div class="modal fade" id="modalmodifForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="" method="post" enctype="multipart/form-data">
        <div class="modal-header infouser text-center">
          <div class="profil">
            <img src="{{ url('main/assets/images/Sanstitre-121.png') }}" style="width:60px; height:60px;border:1px solid black; border-radius:50%;" alt="image">
          </div>
          <div >
            <h4 class="modal-title w-100 font-weight-bold">Paramètres</h4><hr/>
            <div class="file-field medium">
              <div class="file-field">
                <div class="btn btn-outline-success btn-rounded waves-effect btn-sm float-left">
                  <span><input class="file-path validate fil-path" type="text" placeholder="Changez votre photo de profil"></span>
                  <input type="file" name="photoprofil" value="2 * 1024 * 1024">
                </div>
              </div>
            </div>
          </div>
          <div class="closed">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        <div class="modal-body mx-3">
          <div class="md-form">
            <label data-error="wrong" data-success="right">Nom</label>
            <input type="text" id="FirstNameAdmin" name="FirstNameAdmin" class="form-control validate"  placeholder="Non de famille" >
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right" >Prenoms</label>
            <input type="text" id="OtherNameAmin" name="OtherNameAdmin" class="form-control validate" placeholder="Prenoms">
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right" >Date de naissance</label>
            <input type="date" id="date" name="date" class="form-control validate" placeholder="Changez la date de naissance">
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right" >Telephone</label>
            <input type="tel" id="telnumber" name="telnumber" class="form-control validate" placeholder="Entrez le nouveau numero">
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right" >Adresse</label>
            <input type="text" id="textAdresse" name="textAdresse" class="form-control validate" placeholder="Entrez la nouvelle adresse">
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right" >Profession</label>
            <input type="text" id="textProf" name="textProf" class="form-control validate" placeholder="Entrez la nouvelle profession">
          </div>
          <div class="md-form">
            <label data-error="wrong" data-success="right">Mot de passe</label>
            <input type="password" id="Password" name="Password" class="form-control validate" placeholder="Entrer la nouveau Password">
          </div>
          <center>
            <div class="btn btn-outline-success btn-rounded waves-effect btn-sm float-left">
            <span><input class="file-path validate fil-path" type="text" placeholder="Entrez la nouvelle signature"></span>
            <input type="file" name="signature" value="2 * 1024 * 1024">
            </div>
          </center>
          <center><input type="submit" name="changeParamUser" value ="Sauvegarder ses changements" class="btn btn-success buttonAdd"></center>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modalsuppForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="" method="post" enctype="multipart/form-data">
        <div class="modal-header infouser text-center">
          <div >
            <h4 class="modal-title w-100 font-weight-bold">Suppression</h4><hr/>
          </div>
          <div class="closed">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        <div class="modal-body mx-3">
          <div class="modal-body">
            <p>Voulez vraiment supprimez ce compte ?<br/>Etre-vous vraiment sure de cette opération ?</p>
          </div>
          <div class="modal-footer">
            <a class="btn btn-secondary" href="{{ url('/ListComptClient') }}">Annuler</a>
            <button type="button" class="btn btn-success">Oui</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
