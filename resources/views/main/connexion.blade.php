@extends('main/layouts/master', [
'title' => 'Connexion',
'sub_title' => '',
])


@section('content')
<div class="connexion">
    
    <div class="firstsquare">
        
        <center>
        <div >
            <div class="row col-12" style="height:110vh;width:500px;margin-top:200px;">
                <div class="col-12" style="width:500px;">
                    <div>
                        <img class="logo1"  alt="Yes Logo" src="{{ url('main/assets/images/Sanstitre-121.png') }}">
                        <div class="card-body" style="width:500px;">
                            <form action="" autocomplete="off" style="width:350px;">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" placeholder="Identifiant">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" placeholder="mot de passe">
                                </div>
                                <button type="button" id="sendlogin" class="btn btn-outline-warning">CONNECTION</button>
                                <p>Mot de passe oublié ?</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </center>
    </div>
    <div class="secongsquare">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item item-11">
                <div class="img-fill" style="margin-top: 100px;height:85vh;border-raduis:5px;">
                    <div class="text-content">
                        <h4 class="textslide6">CONNECTEZ-VOUS A VOTRE COMPTE<br> COMME VOUS VOULEZ</h4>
                    </div>
                </div>
            </div>
            <div class="item item-6">
                <div class="img-fill" style="margin-top: 100px;height:85vh;border-raduis:5px;">
                    <div class="text-content">
                        <h5 class="ptextslide4">CONS<strong class="strong1">ULTER VOTRE COMPTE SANS</strong> BOUGER !</h3>
                    </div>
                </div>
            </div>
            <!-- Item -->
            <div class="item item-7" style="height:85vh;border-raduis:5px;">
                <div class="img-fill">
                    <div class="text-content">
                        <h4 class="textslide3">PASSEZ A LA <br> BANQUE EN LIGNE</h4>
                    </div>
                </div>
            </div>
            <!-- // Item -->
            <!-- // Item -->
            <!-- Item -->
            <div class="item item-8" style="height:85vh;border-raduis:5px;">
                <div class="img-fill">
                    <div class="text-content">
                        <h4 class="textslide3">A LA PAUSE <br> CONSULTER VOTRE COMPTE</h4>
                    </div>
                </div>
            </div>
           <!--  // Item -->
        </div>
    </div>

    
</div>
@endsection
