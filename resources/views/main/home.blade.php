@extends('main/layouts/master', [
    'title' => 'Accueil',
    'sub_title' => '',
])

@section('content')
    <!-- Page Content -->
    <!-- Banner Starts Here -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item item-1">
                <div class="img-fill">
                    <div class="text-content">
                        <h6 class="textslide1">BIENVENUE CHEZ NOUS</h6>
                        <h4 class="textslide2">LA BANQUE DU FUTUR</h4>
                    </div>
                </div>
            </div>
            <!-- Item -->
            <div class="item item-4">
                <div class="img-fill">
                    <div class="text-content">
                        <h4 class="textslide3">PASSEZ A LA <br> BANQUE EN LIGNE</h4>
                        <p> <a href="#" class="btn btn-outline-success">DEMANDER UN ACCES</a><a href="#" class="btn btn-outline-warning">CONNEXION</a></p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
            <!-- // Item -->
            <!-- Item -->
            <div class="item item-2">
                <div class="img-fill">
                    <div class="text-content">
                        <h4 class="textslide3">ON VOUS ACCOMPAGNE <br> DANS VOS PROJETS</h4>
                        <p class="ptextslide3"> Parce que nous sommes une famille, notre devoir est de, vous accompagnons, vous diriger et vous conseillons dans tous vos projets.</p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
            <!-- Item -->
            <div class="item item-3">
                <div class="img-fill">
                    <div class="text-content">
                        <h4 class="textslide3"><strong class="strong1">IVOIRE BANQUE</strong></h4>
                        <h5><strong class="strong2">Le partenaire idéal</strong></h5>
                    </div>
                </div>
            </div>
            <!-- // Item -->
        </div>
    </div>
    <!-- Banner Ends Here -->
    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2><em>Services</em></h2>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-item">
                        <img src="{{ url('assets/images/') }}" alt="">
                        <div class="down-content">
                            <h4>CREDIT</h4>
                            <p>Vous avez besoin d'un pret pour demarer votre nouvel activité.</p>
                            <a href="" class="filled-button">Voir +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-item">
                        <img src="{{ url('assets/images/service_02.jpg') }}" alt="">
                        <div class="down-content">
                            <h4>EPARGNE</h4>
                            <p>Epargnez votre argent à un taux d'interet à coupez votre souffle.</p>
                            <a href="" class="filled-button">Voir +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-item">
                        <img src="{{ url('assets/images/service_03.jpg') }}" alt="">
                        <div class="down-content">
                            <h4>ASSURANCE</h4>
                            <p>Optez pour une assurance sans tracas pour gerer les risques.</p>
                            <a href="" class="filled-button">Voir +</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="fun-facts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="left-content">
                        <span>POURQUOI DEVENIR CLIENT IVOIR BANQUE</span><br><br>
                        <h3>UN RESEAU DYNAMIQUE ET PUISSANT</h3><br>
                        <h3><em>UN SERVICE PERSONNALISE</em></h3><br>
                        <h3>UNE EQUIPE PROCHE DE VOUS</h3><br>
                        <h3><em>VOS PROJETS SOUTENUS</em></h3><br>
                    </div>
                </div>
                <div class="col-md-6 align-self-center">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">9245</div>
                                <div class="count-title">Avis favorable par semaine</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">15500</div>
                                <div class="count-title">Opération Journaliere</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">15578</div>
                                <div class="count-title">Projets réalisés</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">26</div>
                                <div class="count-title">Récompenses</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="more-info">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="more-info-content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="left-image">
                                    <img src="{{ url('main/assets/images/Sanstitre-121.png') }}" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 align-self-center">
                                <div class="right-content">
                                    <h2>QUI SOMMES <em> NOUS ?</em></h2>
                                    <p>Ivoire Banque Cote d'ivoire a débuté ses activités en juin 2000 sous l'impulsion du GROUPE FOUR qui est le principal actionnaire.<br> Des son entrée sur le marché ivoirien, elle s'est orientée vers le secteurs des PME fortement demandeur de solutions innovantes et vers la jeunesse et les foyers a faible revenus pour relever le taux de pauvrété en Cote d'Ivoire</p>
                                    <a href="#" class="filled-button">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="callback-form">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>CONTACTER <em>NOUS</em></h2>
                        <span>Laissez nous un message</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="contact-form">
                        <form id="contact" action="" method="post">
                            <div class="row">
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <fieldset>
                                        <input name="name" type="text" class="form-control" id="name"
                                            placeholder="Full Name" required="">
                                    </fieldset>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <fieldset>
                                        <input name="email" type="text" class="form-control" id="email"
                                            pattern="[^ @]*@[^ @]*" placeholder="E-Mail Address" required="">
                                    </fieldset>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <fieldset>
                                        <input name="subject" type="text" class="form-control" id="subject"
                                            placeholder="Subject" required="">
                                    </fieldset>
                                </div>
                                <div class="col-lg-12">
                                    <fieldset>
                                        <textarea name="message" rows="6" class="form-control" id="message"
                                            placeholder="Your Message" required=""></textarea>
                                    </fieldset>
                                </div>
                                <div class="col-lg-12">
                                    <fieldset>
                                        <button type="submit" id="form-submit" class="border-button">Send Message</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
