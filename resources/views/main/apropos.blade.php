@extends('main/layouts/master', [
'title' => 'A propos',
'sub_title' => '',
])

@section('content')
    <!-- Page Content -->
    <div class="page-heading header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Nous avons 21 ans expérience dans notre domaine grace à vous</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="more-info about-info">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="more-info-content">
                        <div class="row">
                            <div class="col-md-6 align-self-center">
                                <div class="right-content">
                                    <h2>A PROPOS<em> DE NOUS</em></h2>
                                    <p>Ivoire Banque Cote d'ivoire a débuté ses activités en juin 2000 sous l'impulsion du GROUPE FOUR qui est le principal actionnaire.<br>
                                     Des son entrée sur le marché ivoirien, elle s'est orientée vers le secteurs des PME fortement demandeur de solutions innovantes et
                                      vers la jeunesse et les foyers a faible revenus pour relever le taux de pauvrété en Cote d'Ivoire.
                                        <br>Evoluant dans ce marché bancaire ivoirien très concurrentiel,
                                         IVOIRE BANQUE a su maintenir une place essentielle tout en faisant croître ses activités
                                         et son capital social qui s’élève aujourd’hui à 12.500.000.000 FCFA.
                                         <br/>Au cours de ces dix dernières années, elle a su s’adapter aux différents changements politique,
                                          économique et social. Elle a par ailleurs su intégrer les défis quotidiens,
                                          lui permettant ainsi d’optimiser ses forces et de saisir les opportunités à elle offertes.<br/>
                                        <br/>

                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="left-image">
                                    <img src="{{ url('main/assets/images/Sanstitre-121.png') }}" alt="">
                                    <p>
                                    Forte de 229 collaborateurs au 31 mars 2020, elle est soucieuse de se hisser progressivement
                                          parmi les banques présentant les meilleurs critères de gestion et de rentabilité.
                                           IVOIRE BANQUE Côte d’Ivoire, au-delà de la croissance de son volume d’activité, veille également au strict respect
                                            des standards de la profession et des pratiques de bonne gouvernance se traduisant par:
                                            Le dynamisme et la mobilisation des différentes équipes ;
Le déploiement soutenu et ciblé d’un réseau efficient d’agences adapté à notre clientèle ;
La mise en œuvre d’une politique de gestion rigoureuse du risque, seul gage du maintien de la qualité du portefeuille de BBG CI et de La protection des déposants.
L’ensemble de ces actions est dirigé vers un unique but : la satisfaction de notre clientèle par une écoute toujours attentive et la mise en œuvre de solutions réellement adaptées à ses besoins.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="team">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>NOTRE EQUIPES<em> ADMINISTRATIVES</em></h2>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-item">
                        <img src="assets/images/team_01.jpg" alt="">
                        <div class="down-content">
                            <h4>YEDO MAXIMILIEN KPARO</h4>
                            <span>PRESIDENT DIRECTEUR GENERALE</span>
                            <p>In sem sem, dapibus non lacus auctor, ornare sollicitudin lacus. Aliquam ipsum urna, semper
                                quis.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-item">
                        <img src="assets/images/team_02.jpg" alt="">
                        <div class="down-content">
                            <h4>NOMEL AGNERO ANGE</h4>
                            <span>DIRECTEUR GENERAL</span>
                            <p>In sem sem, dapibus non lacus auctor, ornare sollicitudin lacus. Aliquam ipsum urna, semper
                                quis.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-item">
                        <img src="assets/images/team_03.jpg" alt="">
                        <div class="down-content">
                            <h4>DIEGANA OUMOU</h4>
                            <span>DIRECTRICE DES FINANCES</span>
                            <p>In sem sem, dapibus non lacus auctor, ornare sollicitudin lacus. Aliquam ipsum urna, semper
                                quis.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-item">
                        <img src="assets/images/team_03.jpg" alt="">
                        <div class="down-content">
                            <h4>FOFANA MAMBA</h4>
                            <span>DIRECTRICE RESSOURCES HUMAINES</span>
                            <p>In sem sem, dapibus non lacus auctor, ornare sollicitudin lacus. Aliquam ipsum urna, semper
                                quis.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="fun-facts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="left-content">
                        <br><br><br><br><br><br>
                        <h3>NOS REALISATIONS</h3><br>
                        <h3>NOS RECOMPENSES</h3><br>
                        <h3>NOTRE FIERTE</h3><br><br>
                    </div>
                </div>
                <div class="col-md-6 align-self-center">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">9245</div>
                                <div class="count-title">Avis favorable par semaine</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">15500</div>
                                <div class="count-title">Opération Journaliere</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">15578</div>
                                <div class="count-title">Projets réalisés</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="count-area-content">
                                <div class="count-digit">26</div>
                                <div class="count-title">Récompenses</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
