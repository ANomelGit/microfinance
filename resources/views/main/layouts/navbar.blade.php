<!-- ***** Preloader Start ***** -->
<div id="preloader">
    <div class="jumper">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- ***** Preloader End ***** -->

<!-- Header
<div class="sub-header">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <ul class="left-info">
                    <li><a href="#"><i class="fa fa-clock-o"></i>Lun-Ven 09:00-17:00</a></li>
                    <li><a href="#"><i class="fa fa-phone"></i>090-080-0760</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="right-icons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-behance"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
-->
<header class="">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a href="index.html">
                <img class="logo"  alt="Yes Logo" src="{{ url('main/assets/images/Sanstitre-121.png') }}">
            </a>
            <a href="index.html">
                <h2>IVOIRE BANQUE</h2>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item {{ $title == 'Accueil' ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('home') }}">Accueil
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $title == 'A propos' ? 'active' : '' }}" href="{{ route('apropos') }}">A propos de nous</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $title == 'Nos services' ? 'active' : '' }}" href="{{ route('services') }}">Nos services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $title == 'Contactez-nous' ? 'active' : '' }}" href="{{ route('contact') }}">Contactez nous</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $title == 'Connexion' ? 'active' : '' }}" href="{{ route('connexion') }}">Connexion</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
