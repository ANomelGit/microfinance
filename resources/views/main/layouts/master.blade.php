<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title>Ivoire Banque</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ url('main/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{ url('main/assets/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ url('main/assets/css/templatemo-finance-business.css') }}">
    <link rel="stylesheet" href="{{ url('main/assets/css/owl.css') }}">
<!--

Finance Business TemplateMo

https://templatemo.com/tm-545-finance-business

-->
  </head>

  <body>

    @include('main.layouts.navbar')

    @yield('content')

    <!-- Footer Starts Here -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-3 footer-item">
            <h4>IVOIRE BANQUE</h4>
          </div>
          <div class="col-md-3 footer-item">
            <h4>Liens Utils</h4>
            <ul class="menu-list">
              <li><a href="#">Accueil</a></li>
              <li><a href="#">A Propos De Nous</a></li>
              <li><a href="#">Nos Services</a></li>
            </ul>
          </div>
          <div class="col-md-3 footer-item">
            <h4></h4><br/>
            <ul class="menu-list">
              <li><a href="#">Contactez nous</a></li>
              <li><a href="#">Banque en ligne/Connexion</a></li>
            </ul>
          </div>
          <div class="col-md-3 footer-item">
            <h4>SUIVEZ-NOUS</h4>
            <ul class="social-icons">
              <li><a rel="nofollow" href="https://fb.com/templatemo" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fa fa-behance"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p>Copyright &copy; 2021 Ivoire Banque</p>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ url('main/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('main/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Additional Scripts -->
    <script src="{{ url('main/assets/js/custom.js') }}"></script>
    <script src="{{ url('main/assets/js/owl.js') }}"></script>
    <script src="{{ url('main/assets/js/slick.js') }}"></script>
    <script src="{{ url('main/assets/js/accordions.js') }}"></script>

    <script language = "text/Javascript">
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>
