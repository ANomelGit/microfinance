@extends('main/layouts/master', [
'title' => 'Nos services',
'sub_title' => '',
])

@section('content')
    <!-- Page Content -->
    <div class="single-services">
        <div class="container">
            <div class="row" id="tabs">
                <div class="col-md-4">
                    <ul>
                        <li><a href='#tabs-1'>Epargne <i class="fa fa-angle-right"></i></a></li>
                        <li><a href='#tabs-2'>Pret <i class="fa fa-angle-right"></i></a></li>
                        <li><a href='#tabs-3'>Assurance <i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <section class='tabs-content'>
                        <article id='tabs-1'>
                            <img src="assets/images/single_service_01.jpg" alt="">
                            <h4>Epargne</h4>
                            <p>Nos services d'épargne vous permettent de valoriser vos économies en toutes sécurité
                                tout en les maintenant accessible. Une offre formidable pour vous permettre de gagnez en epargnant
                                votre argent. Epargnez chez nous, vous permet a partir de votre compte vos opérations bancaire courantes
                                DEPOT et RETRAIT d'espece,chèque et virement.
                            </p>
                        </article>
                        <article id='tabs-2'>
                            <img src="assets/images/single_service_02.jpg" alt="">
                            <h4>CREDIT</h4>
                            <p>Une solution de pret idéal o=pour vos projets. Nous vous offrons une aide financiere pour realiser vos projets
                                ,pour acquerir une maison, pour assurer les études de vos enfants en toute sérénité ou encore la voiture  de vos rêves.
                                Notre solution de financement vous accompagne dans la réalisation de vos projets.
                            </p>
                        </article>
                        <article id='tabs-3'>
                            <img src="assets/images/single_service_03.jpg" alt="">
                            <h4>Assurance</h4>
                            <p>Le service Assurance a pour objet de garantir la personne humaine et morale.
                                Il couvre les risques qui portent atteinte à la personne physique ou morale à savoir
                                les pertes financieres, destructions du patrimoine et danger de l'intégrité physique.
                            </p>
                        </article>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="partners">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-partners owl-carousel">
                        <div class="partner-item">
                            <img src="assets/images/client-01.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
