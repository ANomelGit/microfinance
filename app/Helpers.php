<?php

function generateSlug($nom, $pnom, $tel)
{
    return str_replace(' ', '-', $nom . $pnom . '-' . $tel);
}
