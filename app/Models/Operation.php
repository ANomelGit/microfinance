<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    use HasFactory;

    protected $table = 'operations';

    public $timestamps = true;

    protected $fillable = [
        'idoperation',
        'numoperation',
        'dateoperation',
        'montantoperation',
        'visaoperation',
        'obsoperation',
        'idcompte',
        'idagence',
        'idtypeoperation',
        'idmodeoperation',
        'active'
    ];

    public function agence(){
        return $this->belongsTo(Agence::class, 'idagence');
    }

    public function compte(){
        return $this->belongsTo(Compte::class, 'idcompte');
    }

    public function modeoperation(){
        return $this->belongsTo(Modeoperation::class, 'idmodeoperation');
    }

    public function typeoperation(){
        return $this->belongsTo(Typeoperation::class, 'idtypeoperation');
    }
}
