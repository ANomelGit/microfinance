<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compte extends Model
{
    use HasFactory;

    protected $table = 'comptes';

    public $timestamps = true;

    protected $fillable = [
        'idcompte',
        'numcompte',
        'dateouverture',
        'datefermeture',
        'visaouverture',
        'visafermeture',
        'solde',
        'obscompte',
        'active',
        'idclient'
    ];

    public function client(){
        return $this->belongsTo(Client::class, 'idclient');
    }

    public function user(){
        return $this->belongsTo(User::class, 'idclient');
    }

    public function operations(){
        return $this->hasMany(Operation::class, 'idcompte');
    }
}
