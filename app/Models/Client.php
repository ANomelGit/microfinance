<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $table = 'clients';

    public $timestamps = true;

    protected $fillable = [
        'idclient',
        'cni',
        'nomclient',
        'prenomclient',
        'observationclient',
        'profession',
        'telephone',
        'email',
        'datenaissuser',
        'lienphoto',
        'liensignature',
        'active',
        'idsexe'
    ];

    public function comptes(){
        return $this->hasMany(Compte::class, 'idclient');
    }

    public function sexe(){
        return $this->belongsTo(Sexe::class, 'idsexe');
    }
}
