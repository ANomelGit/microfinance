<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sexe extends Model
{
    use HasFactory;

    protected $table = 'sexes';

    public $timestamps = true;

    protected $fillable = [
        'idsexe',
        'libsexe',
        'active'
    ];

    public function clients(){
        return $this->hasMany(Client::class, 'idsexe');
    }
}
