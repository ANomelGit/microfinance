<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agence extends Model
{
    use HasFactory;

    protected $table = 'agences';

    public $timestamps = true;

    protected $fillable = [
        'idagence',
        'libagence',
        'localisationagence',
        'contactagence',
        'active'
    ];

    public function operations(){
        return $this->hasMany(Operation::class, 'idagence');
    }
}
