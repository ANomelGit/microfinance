<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modeoperation extends Model
{
    use HasFactory;

    protected $table = 'modeoperations';

    public $timestamps = true;

    protected $fillable = [
        'idmodeoperation',
        'libmodeoperation',
        'active'
    ];

    public function operations(){
        return $this->hasMany(Operation::class, 'idmodeoperation');
    }
}
