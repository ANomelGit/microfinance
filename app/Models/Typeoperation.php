<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Typeoperation extends Model
{
    use HasFactory;

    protected $table = 'typeoperations';

    public $timestamps = true;

    protected $fillable = [
        'idtypeoperation',
        'libtypeoperation',
        'active'
    ];

    public function operations(){
        return $this->hasMany(Operation::class, 'idtypeoperation');
    }
}
