<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {

    }

    public function infosClient(Request $request)
    {
        $request->validate([
            'cni' => 'required|min:10|string|max:30',
            'nomclient' => 'min:1|max:24|required|string',
            'prenomclient' => 'min:1|max:48|required|string',
            'observationclient' => 'nullable|max:180|string',
            'profession' => 'required|min:1|max:24|string',
            'telephone' => 'required|min:10|max:10|string',
            'email' => 'eamil|required|max:48',
            'datenaissuser' => 'date|required|min|10',
            'lienphoto' => 'required|string|max:180',
            'liensignature' => 'required|string|max:180',
            'idsexe' => 'required|integer'
        ]);

        session([
            'cni' => $request->cni,
            'nomclient' => $request->nomclient,
            'prenomclient' => $request->prenomclient,
            'observationclient' => $request->observationclient,
            'profession' => $request->profession,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'datenaissuser' => $request->datenaissuser,
            'lienphoto' => $request->lienphoto,
            'liensignature' => $request->liensignature,
            'idsexe' => $request->idsexe
        ]);

    }

    public function infosCompte(Request $request)
    {
        $request->validate([
            'numcompte' => 'required|min:10|string|max:30',
            'dateouverture' => 'min:1|max:24|required|string',
            'datefermeture' => 'min:1|max:48|required|string',
            'visaouverture' => 'nullable|max:180|string',
            'visafermeture' => 'required|min:1|max:24|string',
            'solde' => 'required|min:10|max:10|string',
            'active' => 'eamil|required|max:48',
            'idclient' => 'date|required|min|10',
        ]);

        session([
            'numcompte' => $request->cni,
            'dateouverture' => $request->nomclient,
            'datefermeture' => $request->prenomclient,
            'visaouverture' => $request->observationclient,
            'visafermeture' => $request->profession,
            'solde' => $request->telephone,
            'active' => $request->email,
            'idclient' => $request->datenaissuser,
            'iduser' => $request-> Auth::user()->id
        ]);

    }
}
