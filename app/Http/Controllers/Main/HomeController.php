<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('main.home');
    }

    public function apropos(){
        return view('main.apropos');
    }

    public function services(){
        return view('main.services');
    }

    public function contact(){
        return view('main.contact');
    }

    public function connexion(){
        return view('main.connexion');
    }
}
