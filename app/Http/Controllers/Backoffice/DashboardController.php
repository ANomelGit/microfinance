<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        return view('backoffice.dashboard.index');
    }

    public function listeCompteClient() {
        return view('backoffice.CompteClient.ListClient');
    }

    public function typeoperation() {
        return view('backoffice.Operation.typeoperation');
    }

    public function ListBanque() {
        return view('backoffice.MaBanque.ListBanque');
    }

    public function parametre() {
        return view('backoffice.Parametre.parametre');
    }
}
