<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ComptClientController extends Controller
{
    public function ajouter() {
        return view('backoffice.CompteClient.new-infopers');
    }

    public function infosPersonnelles(Request $request) {

        dd($request->all());

        $request->validate([
            'nom' => 'required|min:2|max:255',
            'pnom' => 'required|min:2|max:255',
            'sexe' => 'required|min:1|max:255',
            'tel' => 'required|min:10|max:10',
            'email' => 'email|required|min:5|max:255',
            'cni' => 'string|min:10|max:10',
            'profession' => 'string|min:2|max:255',
            'datenaiss' => 'date|required|min:10|max:10',
            'photo' => 'required|max:12288|min:5|mimes:gif,jpg,jpeg,png,webp',
            'signature' => 'required|max:12288|min:5|mimes:gif,jpg,jpeg,png,webp',
            'obs' => 'nullable|max:1000'
        ]);


        // session([
        //     'nom' => $request->nom,
        //     'pnom' => $request->pnom,
        //     'sexe' => $request->sexe,
        //     'tel' => $request->tel,
        //     'email' => $request->email,
        //     'cni' => $request->cni,
        //     'profession' => $request->profession,
        //     'datenaiss' => $request->datenaiss,
        //     'photo' => $request->photo,
        //     'signature' => $request->signature,
        //     'obs' => $request->obs
        // ]);
    }
    //
}
