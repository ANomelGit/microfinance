<?php

use App\Http\Controllers\Backoffice\BackofficeController;
use App\Http\Controllers\Backoffice\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Main\HomeController;
use App\Http\Controllers\Backoffice\ComptClientController;
use App\Http\Controllers\Backoffice\depotController;
use App\Http\Controllers\Backoffice\retraitController;
use App\Http\Controllers\Backoffice\transfertController;
use App\Http\Controllers\Backoffice\MaBanqueController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Main-login
Route::get('/ connexion', [HomeController::class, 'connexion'])->name('connexion');

// Backoffice Login
Route::get('/auth/login', [BackofficeController::class, 'login'])->name('login');

// Main
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/apropos', [HomeController::class, 'apropos'])->name('apropos');
Route::get('/services', [HomeController::class, 'services'])->name('services');
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');

// Backoffice
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
// Menu Compte Client
Route::get('/ListComptClient', [DashboardController::class, 'listeCompteClient'])->name('ListComptClient');
Route::get('/ajouterClient', [ComptClientController::class, 'ajouter'])->name('ajouterClient');
Route::get('/openAccount', [ComptClientController::class, 'openAccount'])->name('openAccount');
Route::post('/save-infopers', [ComptClientController::class, 'infosPersonnelles'])->name('client.infosPersonnelles');
// Menu les operations
Route::get('/typeoperation', [DashboardController::class, 'typeoperation'])->name('typeoperation');
Route::get('/opdepot', [depotController::class, 'opdepot'])->name('opdepot');
Route::get('/opretrait', [retraitController::class, 'opretrait'])->name('opretrait');
Route::get('/optransfert', [transfertController::class, 'optransfert'])->name('optransfert');
// Menu Ma Banque
Route::get('/CreateBank', [MaBanqueController::class, 'CreateBank'])->name('CreateBank');
Route::get('/ListBanque', [DashboardController::class, 'ListBanque'])->name('ListBanque');
//Menu parametre
Route::get('/parametre', [DashboardController::class, 'parametre'])->name('parametre');
