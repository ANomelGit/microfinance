<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComptesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comptes', function (Blueprint $table) {
            $table->bigIncrements("idcompte");
            $table->string("numcompte", 48);
            $table->date("dateouverture");
            $table->date("datefermeture")->nullable();
            $table->string("visaouverture", 48);
            $table->string("visafermeture", 48);
            $table->decimal("solde", 12,2);
            $table->string("obscompte", 180)->nullable();
            $table->integer("active")->default(1);

            // Cle entrangere

            $table->bigInteger("idclient")->unsigned();
            $table->bigInteger("iduser")->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comptes');
    }
}
