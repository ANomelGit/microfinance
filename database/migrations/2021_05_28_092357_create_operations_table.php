<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->bigIncrements("idoperation");
            $table->string("numoperation", 24);
            $table->date("dateoperation");
            $table->decimal("montantoperation", 12,2);
            $table->string("visaoperation", 24);
            $table->string("obsoperation", 180)->nullable();

            $table->bigInteger("idcompte")->unsigned();
            $table->bigInteger("idagence")->unsigned();
            $table->bigInteger("idtypeoperation")->unsigned();
            $table->bigInteger("idmodeoperation")->unsigned();
            $table->integer("active")->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
