<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->foreign('idsexe')->references('idsexe')->on('sexes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('comptes', function (Blueprint $table) {
            $table->foreign('idclient')->references('idclient')->on('clients')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('iduser')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('operations', function (Blueprint $table) {
            $table->foreign('idcompte')->references('idcompte')->on('comptes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('idagence')->references('idagence')->on('agences')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('idtypeoperation')->references('idtypeoperation')->on('typeoperations')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('idmodeoperation')->references('idmodeoperation')->on('modeoperations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('idrole')->references('idrole')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('idsexe')->references('idsexe')->on('sexes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign('clients_idsexe_foreign');
        });

        Schema::table('comptes', function (Blueprint $table) {
            $table->dropForeign('comptes_idclient_foreign');
            $table->dropForeign('comptes_iduser_foreign');
        });

        Schema::table('operations', function (Blueprint $table) {
            $table->dropForeign('operations_idcompte_foreign');
            $table->dropForeign('operations_idagence_foreign');
            $table->dropForeign('operations_idtypeoperation_foreign');
            $table->dropForeign('operations_idmodeoperation_foreign');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_idrole_foreign');
            $table->dropForeign('users_idsexe_foreign');
        });
    }
}
