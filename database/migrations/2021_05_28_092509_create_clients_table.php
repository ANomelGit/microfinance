<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements("idclient");
            $table->string("cni", 20);
            $table->string("nomclient", 24);
            $table->string("prenomclient", 48);
            $table->string("observationclient", 180)->nullable();
            $table->string("profession", 24);
            $table->string("telephone", 24);
            $table->string("email", 48);
            $table->date("datenaissuser");
            $table->string("lienphoto", 180);
            $table->string("liensignature", 180);
            $table->integer("active")->default(1);

            $table->bigInteger("idsexe")->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
