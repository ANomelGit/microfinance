<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name' => "AGENRO",
            'last_name' => "Nomel Ange Axel",
            'email' => "agneroange1er@gmail.com",
            'tel' => "0787555255",
            'img' => 'img/default_profile.png',
            'adresse' => 'ABIDJAN',
            'idsexe' => 1,
            'slug' => generateSlug('AGENRO', 'Nomel Ange Axel', '0787555255'),
            'idrole' => 1,
            'password' => Hash::make("1234567890")
        ]);
    }
}
