<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        $roles = [
            'gérant',
            'caissière'
        ];

        for ($i=0; $i < 2 ; $i++) {
            Role::create([
                'librole' => $roles[$i]
            ]);
        }
    }
}
